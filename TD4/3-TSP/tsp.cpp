#include <iostream>
#include <sstream>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <fstream>

#include <glpk.h>


std::vector< std::vector<int> > parseTSP(const std::string filename)
{
    std::cout << filename << std::endl;
    std::ifstream is(filename);

    if (!is.is_open()) {
        std::cerr << "Failed to open " << filename << std::endl;
        exit(1);
    } 

    int n;
    is >> n;

    std::vector< std::vector<int> > v(n);

    for (int i=0; i<n; i++) 
    {
        for(int j=0; j<n; j++)   
        {
            int value;
            is >> value;
            v[i].push_back(value);
        }
    }

    return v;
}



std::pair<int, std::vector<std::vector<bool>>>  solveTSP(std::vector< std::vector<int> > T)
{
    glp_prob *lp;
    
    std::vector<int> ia;
    std::vector<int> ja;
    std::vector<double> ar;

	
    double z;
    std::vector<std::vector<bool>> x;


    lp = glp_create_prob();
    glp_set_prob_name(lp, "tsp");
    glp_set_obj_dir(lp, GLP_MIN);




    // définition des colonnes
    int col_count = 0;
    for (int i=0; i < (int) T.size(); i++)
    {
        for (int j=0; j < (int) T.size(); j++)
        {
            if (i!=j)
            {
                std::ostringstream oss;
                oss << "x_" << i << '_' << j;
                col_count++;
                glp_add_cols(lp, 1);
                glp_set_col_name(lp, col_count, oss.str().c_str());
                glp_set_col_kind(lp, col_count, GLP_BV);
                glp_set_obj_coef(lp, col_count, T[i][j]);
            }
        }
    }
    glp_create_index(lp);






    // contraintes
    int row_count = 0;

    // Contrainte 1 : Somme des elements d'une ligne = 1
    for (int i = 0; i < (int) T.size(); i++)
	{
        {
            std::ostringstream oss;
            oss << "C_" << ++row_count;

            glp_add_rows(lp, 1);
            glp_set_row_name(lp, row_count, oss.str().c_str());
            glp_set_row_bnds(lp, row_count, GLP_FX, 1.0, 1.0);
        }

        for (int j = 0; j < (int) T.size(); j++)
        {
            if (i!=j)
            {
                std::ostringstream oss;
                oss << "x_" << i << '_' << j;

                ia.push_back(row_count);
                ja.push_back(glp_find_col(lp, oss.str().c_str()));
                ar.push_back(1.0);
            }
        }
    }


    // Contrainte 2 : Somme des éléments d'une colonne = 1
    for (int j = 0; j < (int) T.size(); j++)
	{
        {
            std::ostringstream oss;
            oss << "C_" << ++row_count;

            glp_add_rows(lp, 1);
            glp_set_row_name(lp, row_count, oss.str().c_str());
            glp_set_row_bnds(lp, row_count, GLP_FX, 1.0, 1.0);
        }

        for (int i = 0 ; i < (int) T.size(); i++)
        {
            if (i!=j)
            {
                std::ostringstream oss;
                oss << "x_" << i << '_' << j;

                ia.push_back(row_count);
                ja.push_back(glp_find_col(lp, oss.str().c_str()));
                ar.push_back(1.0);
            }
        }
    }
    


    // Affichage des x_i_j vrai
    bool changeHasOccured;
    do
    {
        // Ecriture fichier lp
        glp_load_matrix(lp, ia.size() - 1, ia.data(), ja.data(), ar.data());
        glp_write_lp(lp,NULL, "tsp.lp");


        // Result with the new constraint :
        glp_simplex(lp, NULL);
        glp_intopt(lp, NULL);


        // Et récupération des nouvelles valeurs
        z = glp_mip_obj_val(lp);
        std::cout << "Z = " << z << std::endl;

        
        // Récupération des variables x
        x.clear();
        for (int i = 0; i < (int) T.size(); i++)
        {
            std::vector<bool> v;
            for (int j = 0; j < (int) T.size(); j++)
            {
                if (i==j)
                {
                    v.push_back(false);
                }
                else
                {
                    std::ostringstream oss;
                    oss << "x_" << i << '_' << j;
                    v.push_back(glp_mip_col_val(lp, glp_find_col(lp, oss.str().c_str())) == 1);
                }

            }
            x.push_back(v);;
        }





        changeHasOccured = false;

        int start = 0;
        std::vector<int> s;
        s.push_back(start);
        {
            int i=start;


            int j=0;
            while (x[i][j] == false)
            {
                j++;
            }

            while (j != start)
            {
                i=j;
                j=0;
                s.push_back(i);
                
                while (x[i][j] == false)
                {
                    j++;
                }
            }
        }

        // If our graph contains a cycle
        if (s.size() != T.size())
        {
            changeHasOccured = true;

            // Ajout d'une contrainte pour supprimer le cycle
            {
                std::ostringstream oss;
                oss << "C_" << ++row_count;

                glp_add_rows(lp, 1);
                glp_set_row_name(lp, row_count, oss.str().c_str());
                glp_set_row_bnds(lp, row_count, GLP_UP, (int) s.size() - 1, (int) s.size() - 1);
            }
            
            for (int i=0; i < (int) s.size(); i++)
            {

                std::ostringstream oss;
                oss << "x_" << s[i] << '_' << s[(i+1) % (int) s.size()];

                ia.push_back(row_count);
                ja.push_back(glp_find_col(lp, oss.str().c_str()));
                ar.push_back(1);
            }
        }
    }
    while (changeHasOccured);


    // housekeeping
    glp_delete_prob(lp);
    glp_free_env();


	return std::make_pair(z, x);  ;
}





int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Error : call must be like :" << std::endl << "./tsp [filename]" << std::endl;
        exit(1);
    }

    std::vector<std::vector<bool>> x;
    std::vector< std::vector<int> > T = parseTSP(argv[1]);
    std::pair<int, std::vector<std::vector<bool>>> res = solveTSP(T);


    int z = res.first;
    
    x = res.second;

    std::cout << "Z = " << z << std::endl;

    std::cout << "Z = " << z << std::endl;

    // Affichage de la matrice
    for (int i = 0; i < (int) x.size(); i++)
	{
		for (int j = 0; j < (int) x.size(); j++)
		{
			std::cout << x[i][j] << " ";
		}
        std::cout << std::endl;
	}

    // Affichage des variables vraies
    for (int i = 0; i < (int) x.size(); i++)
	{
		for (int j = 0; j < (int) x.size(); j++)
		{
            if (x[i][j])
            {
                std::cout << "x_" << i << "_" << j;
            }
		}
        std::cout << std::endl;
	}

    return 0;
}