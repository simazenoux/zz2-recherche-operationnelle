#include <algorithm>
#include <iostream>
#include <set>
#include <vector>


void generatePatterns(std::set<int> items, int length)
{

}


void displayCutout(std::vector<int> patterns, int L)
{
    std::vector<int> iterators { 0 };

    std::vector<std::vector<int>> patterns;

    std::sort(patterns.begin(), patterns.end());
    std::reverse(patterns.begin(), patterns.end());

    int i = 0;
    int length = 0;


    while (!iterators.empty())
    {
        while (iterators.back() < (int) patterns.size())
        {
            while (length + patterns[iterators.back()] <= L)
            {
                length += patterns[iterators.back()];
                
                // Affichage
                
                std::cout << ++i << " : Length : " << length << " : ";
                for (const auto& i : iterators)
                {
                    std::cout << patterns[i] << " ";
                }
                std::cout << std::endl;

                if (length + patterns[iterators.back()] <= L)
                    iterators.push_back(iterators.back());
            } 

            length -= patterns[iterators.back()];
            ++iterators.back();
        }

        while (iterators.back() >= (int) patterns.size())
        {
            length -= patterns[iterators.back()];
            iterators.pop_back();
        }

        if (!iterators.empty())
        {
            length -= patterns[iterators.back()];
            ++iterators.back();
        }
    }
}


int main(int, char**)
{
    int const L = 100;

    std::set<int> items {14, 31, 45, 36};
    std::vector<int> patterns { 45, 36, 31, 14 };

    displayCutout(patterns, L);
}