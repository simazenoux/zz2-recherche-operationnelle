#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>


using namespace std;

#include "glpk.h"



int main(void)
{
	// les constantes
	const int n =2; //nb de pieces

	const int m = 2; //nb de machines

	int M[3][3];
	int P[3][3];

	 M[1][1] = 1 ;
	 M[1][2] = { 2 };
	 M[2][1] = { 2 };
	 M[2][2] = { 2 };

	 P[1][1] = { 10 };
	 P[1][2] = { 5 };
	 P[2][1] = { 3 };
	 P[2][2] = { 4 };



	// verification solveur
	std::cout << "GLPK version: " << glp_version() << endl;

	// creation des tableaux pour le solveur
	glp_prob *lp;
	int ia[1 + 1000], ja[1 + 1000];
	double ar[1 + 1000], z, x1, x2;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Jobshop");
	glp_set_obj_dir(lp, GLP_MIN);


	// variable globales pour la generation
	int CompteurContrainte = 0;
	int CompteurCol = 0;
	int CompteurIA = 0;
	
	// string nomcol;
	// string nomcontrainte;
	// int position;



	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			std::ostringstream oss;
			oss << "st_" << i << '_' << j;

			CompteurCol++;
			glp_add_cols(lp,1);
			glp_set_col_name(lp, CompteurCol, oss.str().c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
		}
	}

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			for (int k = 1; k <= n; k++)
			{
				for (int p = 1; p <= n; p++)
				{
					std::ostringstream oss;
					oss << "b_" << i << "_" << j << "_" << k << "_" << p;

					CompteurCol++;
					glp_add_cols(lp, 1);
					glp_set_col_name(lp, CompteurCol, oss.str().c_str());
					glp_set_col_kind(lp, CompteurCol, GLP_IV);
					glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
				}
			}
		}
	}

	// creation Index pour les recherches
	glp_create_index(lp);



	// containte 1.
	// -----------
	for (int i = 1; i <= 2; i++)
	{
		for (int j = 2; j <=2; j++)
		{
			glp_add_rows(lp, 1);
			
			{
				std::ostringstream oss;
				oss << "C_" << ++CompteurContrainte;
				glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
				glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, P[i][j-1], P[i][j - 1]);
			}


			{
				std::ostringstream oss;
				oss << "st_" << i <<"_" << j;

				CompteurIA++;
				ia[CompteurIA] = CompteurContrainte; 
				ja[CompteurIA] = glp_find_col(lp, oss.str().c_str());
				ar[CompteurIA] = 1.0;
			}

			{
				std::ostringstream oss;
				oss << "st_" << i << "_" << j-1;
				
				CompteurIA++;
				ia[CompteurIA] = CompteurContrainte; 
				ja[CompteurIA] = glp_find_col(lp, oss.str().c_str());
				ar[CompteurIA] = -1.0;
			}
		}
	}


	// contrainte 2
	const int MAX_INT = 10000;

	for (int i=1; i<=n-1; i++)
	{
		for (int j=1; j<=m; j++)
		{
			for (int k=i+1; k<=n; k++)
			{
				for (int p=1; p<=m; p++)
				{
					if (M[i][j] == M[k][p])
					{
						glp_add_rows(lp, 1);

						{
							std::ostringstream oss;
							oss << "C_" << ++CompteurContrainte;
							glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
							glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, MAX_INT, MAX_INT);
						}

						{
							std::ostringstream oss;
							oss << "st_" << i <<"_" << j;

							CompteurIA++;
							ia[CompteurIA] = CompteurContrainte; 
							ja[CompteurIA] = glp_find_col(lp, oss.str().c_str()); 
							ar[CompteurIA] = - 1.0;
						}
						
						{
							std::ostringstream oss;
							oss << "st_" << k << "_" << p;

							CompteurIA++;
							ia[CompteurIA] = CompteurContrainte; 
							ja[CompteurIA] = glp_find_col(lp, oss.str().c_str());
							ar[CompteurIA] = + 1.0;
						}
						
						{
							std::ostringstream oss;
							oss << "b_" << i << "_" << j << "_" << k << "_" << p;

							CompteurIA++;
							ia[CompteurIA] = CompteurContrainte; 
							ja[CompteurIA] = glp_find_col(lp, oss.str().c_str()); 
							ar[CompteurIA] = - 1.0;
						}
					}
				}
			}
		}
	}


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);
	glp_write_lp(lp, NULL, "partie1.lp");



		

	/*

	// fonction objectif
	// -----------------

	// ajout ligne
	glp_add_rows(lp, 1);
	CompteurContrainte++;
	std::ostringstream oss;
	oss << "C_" << CompteurContrainte;
	nomcontrainte = oss.str();
	glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
	glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);

	// ajout colonne
	//std::ostringstream oss;
	oss.str("");
	oss << "Z";
	nomcol = oss.str();
	glp_add_cols(lp, 1);
	CompteurCol = CompteurCol + 1;
	glp_set_col_name(lp, CompteurCol, nomcol.c_str());
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);

	// creation contrainte partie 1. z = ............
	



	// les coefficients de la fonction objectif
	oss.str("");
	oss << "Z";
	nomcol = oss.str();
	position = glp_find_col(lp, nomcol.c_str());
	glp_set_obj_coef(lp, position, 1);


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "Aliment.lp");

	//glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);


	// valeur de la fonction objectif
	z = glp_mip_obj_val(lp);
	//z = glp_get_obj_val(lp);
	cout << "Z = " << z << endl;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= mp; j++)
		{


		}
	}




	cout << "fin des calculs... appuyez sur une touche" << endl;
	getchar();
	*/
	// liberation mémoire

	glp_delete_prob(lp);
	glp_free_env();


	return 0;
}