#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>
#include <vector>
#include <map>

#include <glpk.h>

#include "MatierePremiere.hpp"
#include "Operation.hpp"
#include "Nutriment.hpp"
#include "ProduitFini.hpp"



using namespace std;

int main(int, char**)
{
	vector<MatierePremiere> matieresPremieres = {
		MatierePremiere("Avoine",  11900, 0.80),
		MatierePremiere("Mais",    23500, 1.00),
		MatierePremiere("Melasse", 750,   0.75)
	};

	vector<Operation> operations = {
		Operation("Mixage",      80),
		Operation("Melange",     30),
		Operation("Granulation", 150),
		Operation("Tamissage",   20)
	};

	std::map <std::string, int> op = {
		{"Mixage",       80},
		{"Melange",      30},
		{"Granulation", 150},
		{"Tamissage",    20}
	};

	vector<Nutriment> nutriments = {
		Nutriment("Proteines", 0.095, 1.00),
		Nutriment("Lipides",   0.020, 1.00),
		Nutriment("Fibres",    0.000, 0.06)
	};

	vector<ProduitFini> produitsFinis = {
		ProduitFini("Granules", 9000),
		ProduitFini("Farine",   12000)
	};

	std::map <std::string, int> pf = {
		{"Granules",  9000},
		{"Farine",   12000}
	};

	vector<vector<double>> composition = {
		{0.136, 0.071, 0.070},
		{0.041, 0.024, 0.037},
		{0.050, 0.003, 0.250}
	};


	// verification solveur
	std::cout << "GLPK version: " << glp_version() << endl;

	// creation des tableaux pour le solveur
	glp_prob *lp;

	int ia[1 + 1000];
	for (int i=0; i< 1002; i++)
	{
		ia[i] = 0;
	}

	std::vector<int> ja;
	ja.push_back(0);

	double ar[1 + 1000];
	double z, x1, x2;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Aliments");
	glp_set_obj_dir(lp, GLP_MIN);



	// variable globales pour la generation
	int CompteurContrainte = 0;
	int CompteurCol = 0;
	int CompteurIA = 0;
	

	// definition des variables 
	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		for (int j = 1; j <= (int) matieresPremieres.size(); j++)
		{
			std::ostringstream oss;
			oss << "x_" << i << '_' << j;
				
			CompteurCol++;
			glp_add_cols(lp,1);
			glp_set_col_name(lp, CompteurCol, oss.str().c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
			
			
			double cost = matieresPremieres[j]._prix + op["Melange"];

			if (i==0)
			{
				cost += op["Granulation"];
			}
			else
			{
				cost += op["Tamissage"];
			}

			if (j!=2)
			{
				cost += op["Mixage"];
			}

			glp_set_obj_coef(lp, CompteurCol, cost);
		}
	}
	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		std::ostringstream oss;
		oss << "q_" << i;
		
		CompteurCol++;
		glp_add_cols(lp,1);
		glp_set_col_name(lp, CompteurCol, oss.str().c_str());
		glp_set_col_kind(lp, CompteurCol, GLP_IV);
		glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
	}

	// creation Index pour les recherches
	glp_create_index(lp);




	// containte 1.
	// -----------

	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		{
			std::ostringstream oss;
			oss << "C_" << ++CompteurContrainte;

			glp_add_rows(lp, 1);
			glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0.0, 0.0);
		}


		{
			std::ostringstream oss;
			oss << "q_" << i;
			
			CompteurIA++;
			ia[CompteurIA] = CompteurContrainte; 
			ja.push_back(glp_find_col(lp, oss.str().c_str()));
			ar[CompteurIA] = -1.0;
		}

		
		for (int j = 1; j <= (int) matieresPremieres.size(); j++)
		{
			std::ostringstream oss;
			oss << "x_" << i << '_' << j;
			
			CompteurIA++;
			ia[CompteurIA] = CompteurContrainte; 
			ja.push_back(glp_find_col(lp, oss.str().c_str()));
			ar[CompteurIA] = 1.0;
		}
	}


	
	
	// contrainte 2 - respect taux minimum en nutriments
	// ------------


	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		for (int k = 1; k <= (int) nutriments.size(); k++)
		{
			if (nutriments[k-1]._teneurMinimum != 0.0)
			{		
				{
					std::ostringstream oss;
					oss << "C_" << ++CompteurContrainte;

					glp_add_rows(lp, 1);
					glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
					glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, 0.0, 0.0);
				}

				
				for (int j = 1; j <= (int) matieresPremieres.size(); j++)
				{
					std::ostringstream oss;
					oss << "x_" << i << '_' << j;

					CompteurIA++;
					ia[CompteurIA] = CompteurContrainte;
					ja.push_back(glp_find_col(lp, oss.str().c_str()));
					ar[CompteurIA] = composition[j-1][k-1];
				}
				
				{
					std::ostringstream oss;
					oss << "q_" << i;

					CompteurIA++;
					ia[CompteurIA] = CompteurContrainte;
					ja.push_back(glp_find_col(lp, oss.str().c_str()));
					ar[CompteurIA] = - nutriments[k-1]._teneurMinimum;
				}
			}
		}
	}


	// contrainte 3 - respect du taux maximum en nutriments
	// ------------


	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		for (int k = 1; k <= (int) nutriments.size(); k++)
		{
			if (nutriments[k-1]._teneurMaximum != 1.0)
			{		
				{
					std::ostringstream oss;
					oss << "C_" << ++CompteurContrainte;

					glp_add_rows(lp, 1);
					glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
					glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, 0.0, 0.0);
				}

				
				for (int j = 1; j <= (int) matieresPremieres.size(); j++)
				{
					std::ostringstream oss;
					oss << "x_" << i << '_' << j;

					CompteurIA++;
					ia[CompteurIA] = CompteurContrainte;
					ja.push_back(glp_find_col(lp, oss.str().c_str()));
					ar[CompteurIA] = composition[j-1][k-1];
				}
				
				{
					std::ostringstream oss;
					oss << "q_" << i;

					CompteurIA++;
					ia[CompteurIA] = CompteurContrainte;
					ja.push_back(glp_find_col(lp, oss.str().c_str()));
					ar[CompteurIA] = - nutriments[k-1]._teneurMaximum;
				}
			}
		}
	}




	// contrainte 4 : respect du stock
	// ------------

	for (int j = 1; j <= (int) matieresPremieres.size(); j++)
	{

		{
			std::ostringstream oss;
			oss << "C_" << ++CompteurContrainte;

			glp_add_rows(lp, 1);
			glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, 0, matieresPremieres[j-1]._quantite);
		}

		
		for (int i = 1; i <= (int) produitsFinis.size(); i++)
		{
			std::ostringstream oss;
			oss << "x_" << i << '_' << j;

			CompteurIA++;
			ia[CompteurIA] = CompteurContrainte;
			ja.push_back(glp_find_col(lp, oss.str().c_str()));
			ar[CompteurIA] = 1;
		}
	}



	// contrainte 5 : respect de la demande
	// ------------

	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		{
			std::ostringstream oss;
			oss << "C_" << ++CompteurContrainte;
			glp_add_rows(lp, 1);
			glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, produitsFinis[i-1]._demande, 0.0); // GLP_LO > | GLP_UP < | GLP_FX =
		}

		{

			std::ostringstream oss;
			oss << "q_" << i;

			CompteurIA++;
			ia[CompteurIA] = CompteurContrainte;
			ja.push_back(glp_find_col(lp, oss.str().c_str()));
			ar[CompteurIA] = 1;
		}
	}
	




	glp_load_matrix(lp, CompteurIA, ia, ja.data(), ar);
	glp_write_lp(lp, NULL, "aliments-betail.lp");



	glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);


	// valeur de la fonction objectif
	z = glp_mip_obj_val(lp);
	//z = glp_get_obj_val(lp);
	cout << "Z = " << z << endl;

	for (int i = 1; i <= (int) produitsFinis.size(); i++)
	{
		for (int j = 1; j <= (int) matieresPremieres.size(); j++)
		{
			std::ostringstream oss;
			oss << "x_" << i << '_' << j;
			
			cout << "x_" << i << '_' << j << " :" << glp_get_col_prim(lp, glp_find_col(lp, oss.str().c_str())) << "\n";
		}
	}



	glp_delete_prob(lp);
	glp_free_env();

	return 0;
}