#ifndef MATIERE_PREMIERE_HPP
#define MATIERE_PREMIERE_HPP

#include <string>

class MatierePremiere
{
    public:
        std::string _nom;
	    int _quantite;
	    float _prix;

    public:
        MatierePremiere(std::string, int, float);
};

#endif