#ifndef NUTRIMENT_HPP
#define NUTRIMENT_HPP	
    
#include <string>

class Nutriment
{
    public:
		std::string _nom;
		double _teneurMinimum;
		double _teneurMaximum;
	
    public:
        Nutriment(std::string nom, double teneurMinimum, double teneurMaximum);
};

#endif