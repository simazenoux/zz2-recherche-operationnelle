#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>

#include <glpk.h>

using namespace std;


int main(int, char**)
{

    std::cout << "GLPK version: " << glp_version() << endl;

    glp_prob *lp;
    int ia[1 + 1000], ja[1 + 1000];
    double ar[1 + 1000], z, x1, x2, x3;

    lp = glp_create_prob();
    glp_set_prob_name(lp, "probleme-test");
    glp_set_obj_dir(lp, GLP_MIN);


    // définition des colonnes
    glp_add_cols(lp, 3);

    glp_set_col_name(lp, 1, "x1");
    glp_set_col_name(lp, 2, "x2");
    glp_set_col_name(lp, 3, "x3");


    glp_set_col_bnds(lp, 1, GLP_LO, 0.0, 0.0);
    glp_set_col_bnds(lp, 2, GLP_LO, 0.0, 0.0);
    glp_set_col_bnds(lp, 3, GLP_LO, 0.0, 0.0);



    // définition des lignes
    glp_add_rows(lp, 3);

    glp_set_row_name(lp, 1, "C1");
    glp_set_row_bnds(lp, 1, GLP_LO, 1.1, 0.0);
    glp_set_row_name(lp, 2, "C2");
    glp_set_row_bnds(lp, 2, GLP_LO, 3.2, 0.0);
    glp_set_row_name(lp, 3, "C3");
    glp_set_row_bnds(lp, 3, GLP_LO, 4.3, 0.0);

    glp_set_obj_coef(lp, 1, 4);
    glp_set_obj_coef(lp, 2, 2);
    glp_set_obj_coef(lp, 3, 1);



    ia[1] = 1, ja[1] = 1, ar[1] = 1.0; /* a[1,1] = 1 */
    ia[2] = 1, ja[2] = 2, ar[2] = 1.0; /* a[1,2] = 2 */
    ia[3] = 2, ja[3] = 2, ar[3] = 2.0; /* a[2,2] = 2 */
    ia[4] = 3, ja[4] = 1, ar[4] = 1.0; /* a[3,1] = 1 */
    ia[5] = 3, ja[5] = 3, ar[5] = 1.0; /* a[3,2] = 2 */



    glp_load_matrix(lp, 5, ia, ja, ar);

    glp_simplex(lp, NULL);

    glp_write_lp(lp,NULL, "probleme-test.lp");

    // obj
    z = glp_get_obj_val(lp);
    
    // les sol
    x1 = glp_get_col_prim(lp, 1);
    x2 = glp_get_col_prim(lp, 2);
    x3 = glp_get_col_prim(lp, 3);


    cout << "z = " << z << endl;
    cout << "x1 = " << x1 << endl;
    cout << "x2 = " << x2 << endl;
    cout << "x3 = " << x3 << endl;


    /* housekeeping */
    glp_delete_prob(lp);
    glp_free_env();


	return 0;
}