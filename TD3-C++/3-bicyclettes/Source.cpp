#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>


using namespace std;

#include "glpk.h"



void main(void)
{
	// les constantes
	const int  n = 12;
	const int CN = 130;  // cout de l'heure normale
	const int CS = 160;  // cout de l'heure sup
	const int S0 = 2; // stock initial
	const int Cst = 20;  // cout de stockage
	const int Capa = 30;

	// les tableaux
	int D[n + 1];

	D[1] = 30;


	

	// verification solveur
	std::cout << "GLPK version: " << glp_version() << endl;

	// creation des tableaux pour le solveur
	glp_prob *lp;
	int ia[1 + 1000], ja[1 + 1000];
	double ar[1 + 1000], z;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Bicyclette");
	glp_set_obj_dir(lp, GLP_MIN);






	// les coefficients de la fonction objectif
	oss.str("");
	oss << "Z";
	nomcol = oss.str();
	position = glp_find_col(lp, nomcol.c_str());
	glp_set_obj_coef(lp, position, 1);


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "Bicyclette.lp");

	glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);


	// valeur de la fonction objectif
	z = glp_mip_obj_val(lp);
	//z = glp_get_obj_val(lp);
	cout << "Z = " << z << endl;

	cout << " --- XN --- " << endl;
	for (int i = 1; i <= n; i++)
	{
		oss.str("");
		oss << "xn_" << i ;
		nomcol = oss.str();
		position = glp_find_col(lp, nomcol.c_str());
		double x = glp_mip_col_val(lp, position);
		cout << "xn_" << i << " :" << x << "\n";
	}

	cout << " --- XS --- " << endl;
	for (int i = 1; i <= n; i++)
	{
		oss.str("");
		oss << "xs_" << i;
		nomcol = oss.str();
		position = glp_find_col(lp, nomcol.c_str());
		double x = glp_mip_col_val(lp, position);
		cout << "xs_" << i << " :" << x << "\n";
	}

	cout << " --- S --- " << endl;
	for (int i = 1; i <= n; i++)
	{
		oss.str("");
		oss << "s_" << i;
		nomcol = oss.str();
		position = glp_find_col(lp, nomcol.c_str());
		double x = glp_mip_col_val(lp, position);
		cout << "s_" << i << " :" << x << "\n";
	}

	cout << "fin des calculs... appuyez sur une touche" << endl;
	getchar();

	// liberation m�moire
	glp_delete_prob(lp);
	glp_free_env();



}